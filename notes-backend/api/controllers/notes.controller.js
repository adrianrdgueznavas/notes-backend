const NoteModel = require('../models/notes.model')
const { handleError } = require('../utils')

module.exports = {
  getAllNotes,
  createNote,
  updateNote,
  deleteNote
}

function getAllNotes (req, res) {
  NoteModel
    .find()
    .then(response => res.json(response))
    .catch((err) => handleError(err, res))
}

function createNote (req, res) {
  NoteModel
    .create(req.body)
    .then((notes) => res.json(notes))
    .catch((err) => handleError(err, res))
}

function deleteNote (req, res) {
  NoteModel
    .findByIdAndDelete(req.params.noteId)
    .then((notes) => res.json(notes))
    .catch((err) => handleError(err, res))
}

function updateNote (req, res) {
  NoteModel
    .findByIdAndUpdate(req.params.noteId, req.body, { new: true })
    .then(updateNotes => res.json(updateNotes))
    .catch((err) => handleError(err, res))
}
