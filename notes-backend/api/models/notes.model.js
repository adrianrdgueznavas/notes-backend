const mongoose = require('mongoose')
const noteSchema = new mongoose.Schema({
  user_Name: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  title: String,
  text: String,
  createdAt: {
    type: Number,
    default: Date.now()
  }

})
const NoteModel = mongoose.model('notes', noteSchema)
module.exports = NoteModel
